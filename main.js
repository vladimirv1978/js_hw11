let formInputButon = document.querySelector(".password-form");
let arr = [...formInputButon.children];
let inputString = [...document.querySelectorAll("input")];
let [entryField] = inputString.slice(-1);

function eventDistribution() {
  arr.forEach((element) => {
    element.addEventListener("click", function (e) {
      switch (e.target.tagName) {
        case "I":
          clickIcon(e);
          break;
        case "BUTTON":
          clickButton(e);
          break;
        case "INPUT":
          clickInput();
          break;
        default:
          break;
      }
    });
  });
}

function clickIcon(e) {
  let arr = e.target.className.split(" ");
  let сlassСheck = arr.some((elem) => elem == "fa-eye");

  if (сlassСheck) {
    e.target.classList.remove("fa-eye");
    e.target.classList.add("fa-eye-slash");
    e.target.previousElementSibling.type = "text";
  } else {
    e.target.classList.remove("fa-eye-slash");
    e.target.classList.add("fa-eye");
    e.target.previousElementSibling.type = "password";
  }
}

function clickButton(e) {
  //Делаем проверку на пустые поля
  let emptyLine = inputString.every((i) => i.value == "");
  if (emptyLine) {
    e.preventDefault();
    return alert("Вы ничего не ввели");
  }

  //Делаем проверку на одинаковые значения
  let stringMatch = inputString.every((i) => i.value == inputString[0].value);
  if (stringMatch) {
    alert("You are welcome");
  } else {
    e.preventDefault();
    if (!document.querySelector(".alert-message")) {
      let button = document.querySelector(".btn");
      entryField.style.marginBottom = "0px";
      button.insertAdjacentHTML(
        "beforebegin",
        '<p style="color:#ff0000; margin:2.5px 0" class="alert-message">Нужно ввести одинаковые значения</p>'
      );
    }
  }
}

function clickInput() {
  let alertMessage = document.querySelector(".alert-message");
  if (alertMessage) {
    entryField.style.marginBottom = "24px";
    alertMessage.remove();
  }
}

eventDistribution();
